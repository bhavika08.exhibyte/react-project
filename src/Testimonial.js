import React, { Component } from 'react'
import Testimonial1 from "./assets/images/testimonial-img1.png";


export class Testimonial extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="row">
                    <div className="container guide-width guide-sec-padding">
                        <div className="container guide-sec-width-wrapper light-yel-tes-bg">
                            <div className="testimonial-title">
                                <h5>What people are saying.</h5>
                            </div>
                            <div className="slider slider-nav testi-slider">
                                <div className="testimonial-wht-box">
                                    <div className="testi-desc">
                                        <p>
                                            “One of my dogs has a hereditary issue. petscreen notified me immediately before completing the rest of the tests. I took Allie to the vet, unknown health issues were fixed and she is now on
                                            preventative treatment"
                                        </p>
                                        <h6>Mia & Benno</h6>
                                    </div>
                                    <div className="testimonial-img">
                                        <img src={Testimonial1} alt="" className="img-fluid" />
                                    </div>
                                </div>
                                <div className="testimonial-wht-box">
                                    <div className="testi-desc">
                                        <p>
                                            “One of my dogs has a hereditary issue. petscreen notified me immediately before completing the rest of the tests. I took Allie to the vet, unknown health issues were fixed and she is now on
                                            preventative treatment"
                                        </p>
                                        <h6>Mia & Benno</h6>
                                    </div>
                                    <div className="testimonial-img">
                                        <img src={Testimonial1} alt="" className="img-fluid" />
                                    </div>
                                </div>
                                <div className="testimonial-wht-box">
                                    <div className="testi-desc">
                                        <p>
                                            “One of my dogs has a hereditary issue. petscreen notified me immediately before completing the rest of the tests. I took Allie to the vet, unknown health issues were fixed and she is now on
                                            preventative treatment"
                                        </p>
                                        <h6>Mia & Benno</h6>
                                    </div>
                                    <div className="testimonial-img">
                                        <img src={Testimonial1} alt="" className="img-fluid" />
                                    </div>
                                </div>
                                <div className="testimonial-wht-box">
                                    <div className="testi-desc">
                                        <p>
                                            “One of my dogs has a hereditary issue. petscreen notified me immediately before completing the rest of the tests. I took Allie to the vet, unknown health issues were fixed and she is now on
                                            preventative treatment"
                                        </p>
                                        <h6>Mia & Benno</h6>
                                    </div>
                                    <div className="testimonial-img">
                                        <img src={Testimonial1} alt="" className="img-fluid" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Testimonial