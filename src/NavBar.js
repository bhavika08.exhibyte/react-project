import React, { Component } from 'react';
import Logo from "./assets/images/logo.png";
import Cartbag from "./assets/images/cart-bag.svg";
import Barico from "./assets/images/bar-icon.svg";
import { NavLink } from 'react-router-dom';

export class NavBar extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="header-fixed">
                    <div className="row header-navigation-wrappar">
                        <div className="col-12">
                            <div className="container">
                                <div className="top-header">
                                    <div className="d-flex flex-column justify-content-between flex-sm-row align-items-center">
                                        <a href="#"><i className="fas fa-map-marker-alt ms-3"></i>United States</a>
                                        <p className="mb-0">FREE shipping on every order!</p>
                                        <ul className="d-flex">
                                            <li>
                                                <a href="#" target="_blank" className="navsign"> Sign in</a>
                                            </li>
                                            <li>
                                                <a href="#" target="_blank">Test activation </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row header-menu-wrappar">
                        <div className="col-12">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className="col-12 col-lg-2">
                                        <div className="usr-hdr d-flex align-items-center justify-content-between">
                                            <div className="usrs-logo">
                                                <NavLink to="/">
                                                    <img src={Logo} alt="" className="img-fluid" />
                                                </NavLink>
                                            </div>
                                            <div className="menu-togle-new">
                                                <div className="hamburger-menu-cust">
                                                    <a href="#">
                                                        <img src={Barico} alt="" />
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-lg-8">
                                        <div className="header-menu-links-inr text-center">
                                            <ul>
                                                <li>
                                                    <NavLink to="/home">Home</NavLink>
                                                </li>
                                                <li>
                                                    <NavLink to="/about">About</NavLink>
                                                </li>
                                                <li>
                                                    <NavLink to="/project">Project</NavLink>
                                                </li>
                                                <li>
                                                    <a href="#">Features</a>
                                                </li>
                                                <li className="mbl-inline-block">
                                                    <a href="#" className="login-btn">About us</a>
                                                </li>
                                                <li className="mbl-inline-block">
                                                    <a href="#" className="sign-btn">FAQ/Help</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-12 col-lg-2 text-end">
                                        <div className="add-to-cart">
                                            <a href="#">
                                                <img src={Cartbag} alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default NavBar