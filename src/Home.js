import React, { Component } from 'react';
import Homeimg from "./assets/images/header-section-img.png";
import Solutionimg from "./assets/images/solution-section-img.png";

export class Home extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="row">
                    <div className="container car-dog-width row-padding-top pb-4 header-section position-relative ">
                        <div className="row align-items-center">
                            <div className="col-12 col-md-6 col-xl-5">
                                <div className="care-dog-description">
                                    <h2>Care smarter. Understanding your dog.</h2>
                                    <p>
                                        petscreen™ helps you know more about your dog so you can give
                                        them the best life possible.
                                    </p>
                                </div>
                                <div className="button-add-to-cart">
                                    <button className="btn btn-orange">Buy Now</button>
                                    <button className="btn btn-grey">How it works</button>
                                </div>
                            </div>
                            <div className="col-12 col-md-6 col-xl-7">
                                <div className="care-dog-img">
                                    <img src={Homeimg} alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* -- ---------------------problem section -------------------------- -- */}
                <div className="row">
                    <div className="container pet-width light-yellow-bg">
                        <div className="row">
                            <div className="col-12 padding-bt-head">
                                <div className="main-title">
                                    <h2 className="mb-0">Understanding your pet’s genetic</h2>
                                </div>
                            </div>
                            <div className="col-12 col-md-4 mt-3">
                                <div className="problem-section text-center">
                                    <div className="problem-sec-img">
                                        <img src="assets/images/cart-bag.svg" alt="" />
                                    </div>
                                    <div className="problem-sec-title">
                                        <h5>210 health tests</h5>
                                        <p>
                                            Identify genetic risks related to drug sensitivities,
                                            vision, weight, mobility, and more.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-4 mt-3">
                                <div className="problem-section text-center">
                                    <div className="problem-sec-img">
                                        <img src="assets/images/cart-bag.svg" alt="" />
                                    </div>
                                    <div className="problem-sec-title">
                                        <h5>Screens for 350+ breeds</h5>
                                        <p>Tests for more breeds than any other dog DNA service.</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-4 mt-3">
                                <div className="problem-section text-center">
                                    <div className="problem-sec-img">
                                        <img src="assets/images/cart-bag.svg" alt="" />
                                    </div>
                                    <div className="problem-sec-title">
                                        <h5>Multi-generational family tree</h5>
                                        <p>
                                            Trace your dog’s ancestry back to their great-grandparents.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <!-- ---------------------solution Section -------------------------- --> */}
                <div className="row">
                    <div className="container car-dog-width row-padding-solution pb-4 position-relative">
                        <div className="row align-items-center">
                            <div className="col-12">
                                <div className="solu-main-title">
                                    <h2 className="mb-0">Your Dogs DNA Test Results</h2>
                                    <p>
                                        Find and connect with other dogs who share your dog’s DNA with the world’s only Canine Relative Finder.
                                    </p>
                                </div>
                            </div>
                            <div className="col-12 col-sm-6 order-change-bottom">
                                <div className="solutions-description">
                                    <h6>Results</h6>
                                    <h2>Ancesstry</h2>
                                    <p>
                                        When you know your pet’s past, you can better plan for their future. We’ve built the world’s largest database of dog and cat DNA. And with that comes industry-leading insights—including breed mix
                                        reporting down to 1%.
                                    </p>
                                    <p>
                                        There’s no better way to get accurate answers about your pet’s ancestral origins.
                                    </p>
                                </div>
                                <div className="button-add-to-cart">
                                    <button className="btn btn-orange">Buy Now</button>
                                </div>
                            </div>
                            <div className="col-12 col-sm-6 order-change-top">
                                <div className="solution-img">
                                    <img src={Solutionimg} alt="" className="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                {/* <!-- ---------------------Guide section-------------------------- --> */}
                <div className="row">
                    <div className="container guide-width guide-sec-padding">
                        <div className="row">
                            <div className="col-12 col-sm-6 col-xl-8">
                                <div className="guide-section-img">
                                    <img src={Homeimg} alt="" className="img-fluid" />
                                </div>
                            </div>
                            <div className="col-12 col-sm-6 col-xl-4">
                                <div className="guide-sec-des">
                                    <h2>DNA Test for Dogs</h2>
                                    <p className="guide-price"><span className="saling-price">99 €</span><span className="offer-price ms-3">119 €</span></p>
                                    <p className="guide-des">
                                        Change the color to match your brand or vision, add your logo, choose the perfect thumbnail, remove the playbar,
                                    </p>
                                    <h6>Features</h6>
                                    <ul>
                                        <li>
                                            <div id="tick-mark"></div>
                                            210+ Genetic Health Risks
                                        </li>
                                        <li>
                                            <div id="tick-mark"></div>
                                            35+ Physical Traits
                                        </li>
                                        <li>
                                            <div id="tick-mark"></div>
                                            350+ Breeds
                                        </li>
                                        <li>
                                            <div id="tick-mark"></div>
                                            Your Dog's Family Tree
                                        </li>
                                    </ul>
                                    <div className="button-add-to-cart pt-4">
                                        <button className="btn btn-orange">Buy Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <!-- ---------------------NEWSLATTER -------------------------- --> */}
                <div className="row newsletter-bg">
                    <div className="container">
                        <div className="newsletter text-center">
                            <h4 className="sub-news-heading">Stay in Touch</h4>
                            <h2 className="main-news-heading">Join our newsletter for pets.</h2>
                        </div>
                        <div className="row">
                            <div className="col-12 col-md-8 col-lg-6 col-xl-5 m-auto">
                                <div className="newsletter-form mt-5">
                                    <div className="input-group">
                                        <input type="search" className="form-control newsletter-input border-0" placeholder="Email" aria-label="Search" aria-describedby="search-addon" />
                                        <button type="button" className="btn search-btn">
                                            Subscribe
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Home