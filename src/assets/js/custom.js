$(function() {
	$(".toggle-icon-main").on("click", function(e) {
		e.preventDefault();
		$(".dash-wrapper").toggleClass("toggled-custom-class");
	});
	
	$(".header-user-profile-inner-cust").on("click", function(e) {
		$(this).toggleClass("active");
		e.stopPropagation();
	});
	$(document).on("click", function(e) {
		if ($(e.target).closest(".header-user-profile-inner-cust").length > 0)
			return;
		if ($(e.target).is(".header-user-profile-inner-cust") === false) {
			$(".header-user-profile-inner-cust").removeClass("active");
		}
	})
	$(window).on('unload', function() {
		$(window).scrollTop(0);
	})

	$('#pls-icon-click').on('click', function() {
		if ($('.pls-ico-inner').hasClass('show-class')) {
			$('.pls-ico-inner').removeClass('show-class');
		} else {
			$('.pls-ico-inner').addClass('show-class');
		}
	})

	var yourNavigation = $(".header-fixed");
	stickyDiv = "header-sticky";
	yourHeader = $('.header-fixed').height();

	$(window).scroll(function () {
		if ($(this).scrollTop() > yourHeader) {
			yourNavigation.addClass(stickyDiv);
		} else {
			yourNavigation.removeClass(stickyDiv);
		}
	});

	$(document).scroll(function () {
		$(this).scrollTop() > 400 ? $(".back-to-top").fadeIn() : $(".back-to-top").fadeOut()
	}), $(".back-to-top").click(function () {
		return $("html, body").animate({
			scrollTop: 0
		}, 1500), !1
	})
})

$(document).ready(function() {
	$(function() {
		$(".menu-togle-new").on("click", function(e) {
			$('.header-menu-links-inr').toggleClass("mbl-menu-open-cust");
			if ($(".header-menu-links-inr").hasClass('mbl-menu-open-cust')) {
				$(this).append('<div class="menu-derk-bg-new"></div>');
			} else {
				$("div.menu-derk-bg-new").remove();
			}
			e.stopPropagation();
		});

		$(document).on("click", function(e) {
			if ($(e.target).closest(".header-menu-links-inr").length > 0)
				return;
			if ($(e.target).is(".header-menu-links-inr") === false) {
				$(".header-menu-links-inr").removeClass("mbl-menu-open-cust");
				$("div.menu-derk-bg-new").remove();
			}
		});

		$(document).on("click", function(e) {
			if ($(e.target).closest(".hamburger-menu-cust").length > 0)
				return;
			if ($(e.target).is(".hamburger-menu-cust") === false) {}
		});
	});
});

$('.slider-nav').slick({
	slidesToShow: 3,
	dots: true,
	infinite: false,
	speed: 300,
	arrows: false,
	focusOnSelect: true,
	responsive: [
		{
		  breakpoint: 1024,
		  settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
			infinite: true,
			dots: true
		  }
		},
		{
			breakpoint: 992,
			settings: {
			  slidesToShow: 2,
			  slidesToScroll: 2
			}
		  },
		{
		  breakpoint: 768,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		  }
		}
	  ]
  });
// jQuery(function ($) {
//     var path = window.location.href; //active menu mate ni js 
//     $('.header-menu-links-inr ul li').each(function () {
//         if (this.href === path) {
//             $(this).parent().addClass('active');
//         }
//     });
// });

// active menu mate ni js 
// $(document).ready(function () {
// 	$('.header-menu-links-inr ul li').click(function () {
// 		$('.header-menu-links-inr ul li').removeClass('active');
// 		$(this).addClass('active');
// 	});
// });