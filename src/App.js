import logo from './logo.svg';
import './App.css';
import './assets/css/style.css';
import Home from './Home.js';
import About from './About.js';
import Project from './Project.js';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import NavBar from './NavBar';
import Footer from './Footer';


function App() {
  return (
    <div className="App" >
      <NavBar />
      <Routes>
        <Route path='/'></Route>
        <Route path='/home' element={<Home />}></Route>
        <Route path='/about' element={<About />}></Route>
        <Route path='/project' element={<Project />}></Route>
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
