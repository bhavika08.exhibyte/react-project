import React, { Component } from 'react'

export class Footer extends Component {
    render() {
        return (
            <React.Fragment>
                <footer className="footer-bg mt-auto">
                    <div className="container d-flex justify-content-between">
                        <span> Copyright © 2021 cerascreen LLC. All rights reserved. </span>
                        <ul className="condition-link">
                            <li className="d-inline-block me-3">
                                <a href="#">Privacy Policy</a>
                            </li>
                            <li className="d-inline-block">
                                <a href="#">Terms of Service</a>
                            </li>
                        </ul>
                    </div>
                </footer>
            </React.Fragment>
        )
    }
}

export default Footer